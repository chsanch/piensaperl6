# Piensa en Perl 6 -- https://uzluisf.gitlab.io/piensaperl6/

# Revisión

Aunque existen muchas cosas[^1] que se pueden/deben tomar en cuenta durante el 
proceso de revisión, la presente revisión se enfocará en los siguientes puntos:

* Eliminar los errores y las imprecisiones de vocabulario.
* Aumentar la riqueza léxica y eliminar muletillas y vicios léxicos.
* Corregir los errores gramaticales y ajustar el texto a las normas y
  a los usos asentados.
* Solventar las inconsistencias sintácticas (concordancia, correlación de
  tiempos verbales, régimen preposicional, etc.); darle mayor fluidez y
  adecuación al texto mediante la elección de recursos sintácticos precisos y
  bien trabajados (conectores del discurso, oraciones subordinadas, eliminación
  de pleonasmos, etc.).
* Corregir los errores ortográficos y de puntuación.


[^1]: https://marianaeguaras.com/correccion-de-estilo-y-ortotipografica-diferencias/

## Colaboración

Si quieres colaborar, puedes escoger cualquier capítulo que no haya sido elegido
por otra persona. Es recomendado que, después de elegir un capítulo, te cerciores de
de agregar tu nombre en la columna **Tomado por** de la tabla más abajo y 
actualizar el archivo README . Esto es para asegurarnos de que solo una persona
está revisando/corrigiendo un capítulo en particular y así distribuir cualquier 
esfuerzo efectivamente. Una vez hayas finalizado con un capítulo, agrega `~~` 
alrededor del nombre del mismo, así que `~~Funciones~~` resulta en ~~Funciones~~.


| Capítulo                                             | Tomado por  |
| --------                                             | --------    |
| 1. La Forma del Programa                             |             |
| 2. Variables, Expressiones y Sentencias              |             |
| 3. Funciones                                         |             |
| 4. Bucles, Condicionales y Recursión                 |             |
| 5. Subrutinas Fructuosas                             |             |
| 6. Iteración                                         |             |
| 7. Cadenas de Texto                                  | ~~uzluisf~~ |
| 8. Caso Práctico: Juego de Palabras                  |             |
| 9. Arrays y Listas                                   |             |
| 10. Hashes                                           |             |
| 11. Caso Práctico: Selección de Estructuras de Datos |             |
| 12. Clases y Objectos                                |             |
| 13. Regexes y Gramáticas                             |             |
| 14. Programación Funcional en Perl                   | uzluisf     |
| 15. Algunos Consejos Finales                         |             |
| A.  Soluciones a los Ejercicios                      |             |


***


Archivo LATEX de la traducción en español de Think Perl 6.

Traducción por Luis F. Uceta.

La versión original en inglés por Laurent Rosenfeld con Allen Downey puede
encontrarse [aquí](http://greenteapress.com/wp/think-perl-6/).

**Descarga directa**: [piensaperl6.pdf](https://gitlab.com/uzluisf/piensaperl6/raw/master/piensaperl6.pdf)

Si deseas el PDF con la última revisión, sigue las instrucciones en la
siguiente sección.

### Preámbulo

Para realizar el resaltado de sintaxis, se usa el paquete `minted`. Dicho
paquete necesita que la librería de Python `Pygments` esté instalada. Si no
está instalada en tu systema, puedes hacerlo con este comando: `pip install pygments`.

El programa `minted` usa uno de los estilos proveídos en este repo
(directorio `pygments`). Para que tal estilo sea aplicado al código, debes 
hacer lo siguiente:

1. Copiar los estilos en el directorio `pygments` al directorio de Pygments en
tu sistema. En mi caso, el directorio destinatario es
`/usr/lib/python3.6/site-packages/pygments/styles/`. Si en tu caso es el mismo,
puedes ejecutar desde `pygments` el comando:
`sudo cp *.py /usr/lib/python3.6/site-packages/pygments/styles/`.

2. Si deseas utilizar otro estilo, puedes especificarlo en
`libro/prelude.sty` dentro de `\newminted` (por ejemplo, `style=bn`).


### Cómo crear el PDF
El directorio `libro` contiene los archivos LATEX necesarios para
compilar el libro. Para recompilar el libro, ejecuta el siguiente
comando dentro de dicho directorio:
```
make
```
Este comando creará el directorio `tmpDir` donde se encuentra el
PDF (piensaperl6.pdf) del libro. 

> Nota: La posibilidad de una compilación exitosa incrementa si tiene una
> instalación casi completa de una distribución reciente de Tex Live.

Para eliminar todos los archivos intermedios (incluyendo el PDF)
que resultan del proceso de compilación, ejecuta:
```
make clean
```
Si deseas mantener el PDF, puedes ejecutar `make update` (antes de ejecutar de
`make clean`) lo cual copiará el PDF en la raíz del directorio `piensaperl6`.

