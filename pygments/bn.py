# -*- coding: utf-8 -*-
"""
    pygments.styles.bn
    ~~~~~~~~~~~~~~~~~~~~~
    :license: BSD, see LICENSE for details.
"""

from pygments.style import Style
from pygments.token import Keyword, Name, Comment, String, Error, \
    Number, Operator, Whitespace


class BnStyle(Style):

    background_color = "#ffffff"
    default_style = ''

    styles = {
        Whitespace:            '#ffffff',
        Comment:               '#000000',
        String:                '#000000',
        Number:                '#000000',
        Operator:              '#000000',
        Keyword:               'bold #000000',
        Keyword.Constant:      '',
        Name.Builtin:          '#000000',
        Name.Variable:         'bold #000000',
        Name.Variable.Global:  'bold #000000',
        Error:                 '#FF0000'
    }
